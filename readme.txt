myshell user manual

Starting
    A make file has been supplied, all the user needs to do is have GNU make installed, then navigate to
    the directory MyShell, then enter the command "make".  The project will be compiled and linked,
    the user then needs to enter the command: ./out/myshell in order to run myshell.

Commands
    1. cd:  cd <dir> will change the current working directory to the supplied director,
            relative to the cwd.  If an error occurs it will be reported.
    2. clr: Will clear the screen
    3. dir: dir <dir> will display the contents of the supplied directory relative to the cwd.
            If <dir> is null the contents of the cwd will be displayed.  Optionally the user may
            type use the command ls <dir> which functions the same.
    4.echo: echo <comment> will output the entire contents of <comment> onto the screen.
    5.quit: ends the myshell process.

    6. all other:   Any other command will be interpreted as a program invocation.  The first supplied
                    argument will be interpreted as the path to the program.  If the final argument
                    is '&' the program will be executed as a background process.

Note:
    A test program named "test" is supplied in the same directory as the readme.  It can be
    it can be invoked by supplying the command "./test".  This program will simply
    ask the user to type a message, and will then repeat that message before terminating.
