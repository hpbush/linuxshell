#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void main()
{

    printf("Enter a msg: ");
    char *p;
    int n;

    int errno = 0;
    n = scanf("%m[a-z]", &p);
    if (n == 1) {
        printf("read: %s\n", p);
        free(p);
    } else if (errno != 0) {
        perror("scanf");
    } else {
        fprintf(stderr, "No matching characters\n");
    }
}
