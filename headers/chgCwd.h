#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "./errMsg.h"

void chgCwd(char ** args);
void syserrmsg(char * msg1, char * msg2);
void errmsg(char * msg1, char * msg2);
