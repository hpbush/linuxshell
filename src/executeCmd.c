#include "../headers/executeCmd.h"

void executeCmd(char ** args, bool bgExecute){
    int status;
    pid_t child_pid;

    switch (child_pid = fork()) {
        case -1:
            syserrmsg("fork",NULL);
            break;
        case 0:
            //execution in child process
            execvp(args[0], args);
            syserrmsg("exec failed -",args[0]);
            exit(127);
    }
    // continued execution in parent process
    if (!bgExecute) waitpid(child_pid, &status, WUNTRACED);
}
