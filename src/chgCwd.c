#include "../headers/chgCwd.h"
#include "../headers/printCwd.h"

#define MAX_BUFFER 1024                        // max line buffer

void chgCwd(char ** args){
    char cwdbuf[MAX_BUFFER];
    //printf(" %s %s\n", args[1], args[2] );

    if (!args[1]) {            // no directory argument
        if (getcwd(cwdbuf, sizeof(cwdbuf)))
            printCwd();
        else
            syserrmsg("retrieving cwd",NULL);
    }else{
        //change the cwd to the supplied dir

        if(chdir(args[1]) == 0){
            //chdir success do nothing
        }else{
            //report error
            printf("Error: %d, %s\n",errno,strerror(errno));
        }
    }
}
