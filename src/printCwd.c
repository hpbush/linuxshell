#include "../headers/printCwd.h"

#define MAX_BUFFER 1024
#define KGRN "\x1B[32m"
#define KNRM "\x1B[0m"

void printCwd(){
  char * prompt = ": ";
	char cwdbuf[MAX_BUFFER];
	if(getcwd(cwdbuf, sizeof(cwdbuf)))   // read current working directory
			printf("%s%s%s%s", KGRN, cwdbuf, KNRM, prompt);   // output as prompt
}
