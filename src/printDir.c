#include "../headers/printDir.h"

void printDir(){
    DIR * dir;
    struct dirent *dp;
    char * file_name;
    dir = opendir(".");
    while((dp=readdir(dir)) != NULL){
        if(!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..")){
            //do not print . or ..
        }else
            printf("%s    ", dp->d_name);
    }
    closedir(dir);
}
