#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "../headers/printCwd.h"
#include "../headers/chgCwd.h"
#include "../headers/printDir.h"
#include "../headers/executeCmd.h"

#define MAX_BUFFER 1024                        // max line buffer
#define MAX_ARGS 64                            // max # args
#define SEPARATORS " \t\n"

void clearBuffer(char ** args, char ** arg);
void printBuffer(char ** args, char ** arg);
bool bgExecute(char ** args, char ** arg);

void main(/*int argc, char ** argv*/)
{

	FILE * ostream = stdout;                   // (redirected) o/p stream
	char linebuf[MAX_BUFFER];                  // line buffer
	char * args[MAX_ARGS];                     // pointers to arg strings
	char ** arg;                               // working pointer thru args
	int i;                                     // working index

	while (!feof(stdin)) {
		printCwd();
	// get command line from input
		if (fgets(linebuf, MAX_BUFFER, stdin )) { // read a line
	// tokenize the input into args array
			arg = args;
			clearBuffer(args,arg);
			arg[0] = strtok(linebuf,SEPARATORS);   // tokenize input
			i = 1;
			while ((arg[i] = strtok(NULL,SEPARATORS))) // last entry will be NULL
				 i++;
			if(arg[0] == NULL) //no argument supplied, continue to avoid crashing
				continue;
			if(strcmp(arg[0],"cd")==0){
				 chgCwd(arg);
			}else if(strcmp(arg[0],"clr")==0){
				 system("clear");
				 continue;
			}else if(strcmp(arg[0],"dir")==0 || strcmp(arg[0],"ls")==0){ //allow support of dir or ls 
				 printDir();
			}else if(strcmp(arg[0],"echo")==0){
				 i = 1;
				while(arg[i])
				 	printf("%s ", arg[i++]);
			}else if(strcmp(arg[0],"quit")==0){
				break;

			 }else{
				 //program invocation.
				 //fork and execute the program as child
				 executeCmd(args, bgExecute(args,arg));
			 }
			 printf("\n");
		}
	}
}

void clearBuffer(char ** args, char ** arg){
	for(int index = 0; index < sizeof(args); index++){
		arg[index] = NULL;
	}
}

bool bgExecute(char ** args, char ** arg){
	for(int index = sizeof(args)-1; index >=0; index--){
		if(arg[index] != NULL){
			if(arg[index][0] == '&')
				return true;
			return false;
		}
	}
	return false;
}

void printBuffer(char ** args, char ** arg){
	printf("\n");
	for(int index = 0; index < sizeof(arg); index++){
		printf("buffer: %s\n",arg[index]);
	}
	printf("\n");
}
